// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getFirestore, collection, addDoc } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-firestore.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCoTj2mBI0sOYPDsRErcAIyrRI5VYKRfqU",
    authDomain: "proyecto-final-6cba4.firebaseapp.com",
    projectId: "proyecto-final-6cba4",
    storageBucket: "proyecto-final-6cba4.appspot.com",
    messagingSenderId: "787498874785",
    appId: "1:787498874785:web:ad512c98257b0eb03c3de0"
};

// Initialize Firebase
const firebase = initializeApp(firebaseConfig);

const db = getFirestore(firebase);

// La función saveProductos se define como una función asíncrona que recibe un objeto 'producto' como argumento
const saveProductos = async (producto) => {
    try {
        // Utiliza la función addDoc de Firebase para agregar un nuevo documento en la colección 'productos'
        // La función devuelve una referencia al documento recién creado
        const docRef = await addDoc(collection(db, "productos"), producto);

        // Llama a la función MSJOk para mostrar un mensaje de éxito
        MSJOk();
    } catch (error) {
        // Si ocurre algún error durante la operación, llama a la función MSJERR para mostrar un mensaje de error
        MSJERR();
    }
}


const MSJOk = () => {
    Swal.fire({
        title: "Todo en orden!",
        text: "Datos guardados correctamente!",
        icon: "success"
    });
}

const MSJERR = () => {
    Swal.fire({
        title: "Ooops!",
        text: "Eror al guardar los datos!",
        icon: "error"
    });
}


const btnGuardar = document.getElementById('btnGuardar');
//Funcion para agregar el producto
btnGuardar.onclick = () => {
    let nombre_producto = document.getElementById('nombre').value;
    let desc_Producto = document.getElementById('descripcion').value;
    let precio_producto = document.getElementById('precio').value;
    let img_producto = document.getElementById('img').value;
    let categoria = document.getElementById('categoria');
    let categoriaSelect = categoria.value;

    //Validar los campos que no esten vacios
    if (!nombre_producto || !desc_Producto || !precio_producto || !img_producto) {
        alert("Debe llenar todos los campos");
    } else {
        //Se crea el objeto con los datos a insertar en la base de datos
        const producto = {
            nombre_producto,
            descripcion: desc_Producto,
            precio: precio_producto,
            imagen: img_producto,
            categoria: [categoriaSelect]
        };
        //Vaciar todos los campos
        document.getElementById('nombre').value = '';
        document.getElementById('descripcion').value = '';
        document.getElementById('precio').value = '';
        document.getElementById('img').value = '';

        saveProductos(producto);
    }
};

//Funcion para regresar
const btnRegresar = document.getElementById('return');
btnRegresar.addEventListener('click', ()=>{
    window.location.href="../html/panelAdmin.html";
});

