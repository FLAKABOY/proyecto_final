// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getFirestore, collection, getDocs, updateDoc, query, where } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-firestore.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCoTj2mBI0sOYPDsRErcAIyrRI5VYKRfqU",
    authDomain: "proyecto-final-6cba4.firebaseapp.com",
    projectId: "proyecto-final-6cba4",
    storageBucket: "proyecto-final-6cba4.appspot.com",
    messagingSenderId: "787498874785",
    appId: "1:787498874785:web:ad512c98257b0eb03c3de0"
};

// Initialize Firebase
const firebase = initializeApp(firebaseConfig);
const db = getFirestore(firebase);


document.addEventListener('DOMContentLoaded', function () {
    // Obtiene los datos del producto almacenados en localStorage
    const productoDataString = localStorage.getItem('productoData');

    // Convierte la cadena JSON a un objeto JavaScript
    const productoData = JSON.parse(productoDataString);

    // Utiliza los datos según sea necesario (puedes llenar los campos del formulario, por ejemplo)
    document.getElementById('nombre').value = productoData.nombre;
    document.getElementById('descripcion').value = productoData.descripcion;
    document.getElementById('precio').value = productoData.precio;
    document.getElementById('img').value = productoData.imagen;
    switch (productoData.categoria) {
        case 'bebida': {
            // Obtén el elemento select por su id
            const selectElement = document.getElementById('categoria');

            // Crea un nuevo elemento option para Bebida
            const optBebida = document.createElement('option');
            const optAlimento = document.createElement('option');
            const optPostre = document.createElement('option');
            // Asigna el texto y el valor de la opción
            optBebida.text = 'Bebida';
            optBebida.value = 'bebida';
            optAlimento.text = 'Alimento';
            optAlimento.value = 'alimento';
            optPostre.text = 'Postre';
            optPostre.value = 'postre';
            // Agrega la opción al select
            selectElement.add(optBebida);
            selectElement.add(optAlimento);
            selectElement.add(optPostre);
            break;
        }
        // Repite para otras categorías según sea necesario
        case 'alimento': {
            const selectElement = document.getElementById('categoria');


            const optAlimento = document.createElement('option');
            const optPostre = document.createElement('option');
            const optBebida = document.createElement('option');

            optAlimento.text = 'Alimento';
            optAlimento.value = 'alimento';
            optPostre.text = 'Postre';
            optPostre.value = 'postre';
            optBebida.text = 'Bebida';
            optBebida.value = 'bebida';

            selectElement.add(optAlimento);
            selectElement.add(optPostre);
            selectElement.add(optBebida);
            break;
        }
        case 'postre': {
            const selectElement = document.getElementById('categoria');

            const optPostre = document.createElement('option');
            const optAlimento = document.createElement('option');
            const optBebida = document.createElement('option');

            optPostre.text = 'Postre';
            optPostre.value = 'postre';
            optAlimento.text = 'Alimento';
            optAlimento.value = 'alimento';
            optBebida.text = 'Bebida';
            optBebida.value = 'bebida';

            selectElement.add(optPostre);
            selectElement.add(optAlimento);
            selectElement.add(optBebida);
            break;
        }

    }
});

const MSJOk = () => {
    Swal.fire({
        title: "Todo en orden!",
        text: "Datos Actualizados correctamente!",
        icon: "success"
    });
}

const MSJERR = () => {
    Swal.fire({
        title: "Ooops!",
        text: "Eror al Actualizar los datos!",
        icon: "error"
    });
}

// Agrega un evento de clic al botón de editar
const btnEdit = document.getElementById('btnEdit');
document.addEventListener('DOMContentLoaded', function () {
    btnEdit.addEventListener('click', async function () {
        //console.log("Editando...");
        // Obtiene los nuevos valores de los campos del formulario
        const nuevoNombre = document.getElementById('nombre').value;
        const nuevaDescripcion = document.getElementById('descripcion').value;
        const nuevoPrecio = document.getElementById('precio').value;
        const nuevaImagen = document.getElementById('img').value;
        const nuevaCategoria = document.getElementById('categoria').value;

        // Obtiene el nombre antiguo del producto almacenado en localStorage
        const productoDataString = localStorage.getItem('productoData');
        const productoData = JSON.parse(productoDataString);
        const nombreAntiguo = productoData.nombre;

        // Encuentra el documento en la base de datos con el nombre del producto
        const productosCollection = collection(db, 'productos');
        const q = query(productosCollection, where('nombre_producto', '==', nombreAntiguo));
        const productosSnapshot = await getDocs(q);

        if (!productosSnapshot.empty) {
            const productoDoc = productosSnapshot.docs[0];

            // Actualiza el documento en la base de datos
            await updateDoc(productoDoc.ref, {
                nombre_producto: nuevoNombre,
                descripcion: nuevaDescripcion,
                precio: nuevoPrecio,
                imagen: nuevaImagen,
                categorias: nuevaCategoria
                // Agrega otros campos según sea necesario
            });

            // Puedes redirigir o mostrar un mensaje de éxito aquí
            MSJOk();
            // Espera 2 segundos (o el tiempo que desees) antes de redirigir
            setTimeout(() => {
                window.location.href = '../html/panelAdmin.html';
            }, 2000);
        } else {
            MSJERR();
        }
    });
});

//Funcion para regresar
const btnRegresar = document.getElementById('return');
btnRegresar.addEventListener('click', ()=>{
    window.location.href="../html/panelAdmin.html";
});
