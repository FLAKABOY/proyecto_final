//Obtener los elementos por id
const btnBebidas = document.getElementById('btnBebidas');
const btnAlimentos = document.getElementById('btnAlimentos');
const btnPostres = document.getElementById('btnPostres');

//Funcion que redirige a la pagina de menu de las bebidas
//---------------------------------------------------------
btnBebidas.addEventListener('click', async function (){
    window.location = "../html/bebidas.html";
});

//---------------------------------------------------------

//Funcion que redirige a la pagina de menu de los postres
//---------------------------------------------------------
btnPostres.addEventListener('click', async function (){
    window.location = "../html/postres.html";
});
//---------------------------------------------------------

//Funcion que redirige a la pagina de menu de los platos principales
//---------------------------------------------------------
btnAlimentos.addEventListener('click', async function (){
    window.location = "../html/alimentos.html";
    });
//---------------------------------------------------------

