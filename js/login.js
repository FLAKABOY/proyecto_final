// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";
import { getFirestore, collection, getDocs, query, where } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-firestore.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCoTj2mBI0sOYPDsRErcAIyrRI5VYKRfqU",
    authDomain: "proyecto-final-6cba4.firebaseapp.com",
    projectId: "proyecto-final-6cba4",
    storageBucket: "proyecto-final-6cba4.appspot.com",
    messagingSenderId: "787498874785",
    appId: "1:787498874785:web:ad512c98257b0eb03c3de0"
};

// Initialize Firebase
const firebase = initializeApp(firebaseConfig);

// Obtén una instancia de autenticación
const auth = getAuth(firebase);
const db = getFirestore(firebase);


const MSJLOGIN = () => {
    Swal.fire({
        title: "Todo en orden!",
        text: "Bienvenido",
        icon: "success"
    });
}

const MSJERR = () => {
    Swal.fire({
        title: "Error",
        text: "Usuario o contraseña incorrectos",
        icon: "error"
    });
}

// Selecciona el formulario y agrega un evento de escucha para la presentación del formulario
document.querySelector('form').addEventListener('submit', async function (e) {
    e.preventDefault(); // Evita el envío normal del formulario

    // Obtiene los valores de nombre de usuario y contraseña del formulario
    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;

    try {
        // Consulta la colección de usuarios para encontrar el usuario con el nombre de usuario
        const usersCollection = collection(db, 'usuarios');
        const usersQuery = query(usersCollection, where('usuario', '==', username));
        const userSnapshot = await getDocs(usersQuery);

        if (userSnapshot.size > 0) {
            // Usuario encontrado en la base de datos
            const userData = userSnapshot.docs[0].data();

            // Verifica la contraseña
            if (userData.contrasena === password) {
                // Establece un token de sesión o cookie para indicar que el usuario ha iniciado sesión

                // Inicio de sesión exitoso
                MSJLOGIN();

                // Redirige al usuario a otra página (por ejemplo, "panelAdmin.html")
                window.location.href = '../html/panelAdmin.html';
            } else {
                // Contraseña incorrecta
                MSJERR();
            }
        } else {
            // Usuario no encontrado en la base de datos
            MSJERR();
        }
    } catch (error) {
        console.error("Error durante el inicio de sesión:", error);
        MSJERR();
    }
});
