// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getFirestore, collection, getDocs, query, where, deleteDoc } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-firestore.js";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCoTj2mBI0sOYPDsRErcAIyrRI5VYKRfqU",
    authDomain: "proyecto-final-6cba4.firebaseapp.com",
    projectId: "proyecto-final-6cba4",
    storageBucket: "proyecto-final-6cba4.appspot.com",
    messagingSenderId: "787498874785",
    appId: "1:787498874785:web:ad512c98257b0eb03c3de0"
};

// Initialize Firebase
const firebase = initializeApp(firebaseConfig);
const db = getFirestore(firebase);

// Función para cargar los datos en la tabla
const cargarDatosEnTabla = async (categoria, tablaId) => {
    console.log(`Cargando datos para la categoría: ${categoria}`);
    try {
        // Consulta la colección de productos para la categoría específica
        const productosCollection = collection(db, 'productos');
        const productosQuery = query(productosCollection, where('categoria', 'array-contains', categoria));
        const productosSnapshot = await getDocs(productosQuery);

        console.log(`Número de productos encontrados para ${categoria}: ${productosSnapshot.size}`);

        // Obtiene la tabla HTML por su ID
        const tabla = document.getElementById(tablaId);
        // Limpia la tabla antes de agregar nuevos datos
        tabla.innerHTML = '';

        // Añade una fila para los títulos de las celdas
        const tituloFila = tabla.insertRow();
        tituloFila.innerHTML = '<th>Imagen</th><th>Nombre</th><th>Descripción</th><th>Precio</th><th>Acciones</th>';

        // Itera sobre los documentos de la consulta y agrega filas a la tabla
        productosSnapshot.forEach((doc) => {
            const productoData = doc.data();
            console.log(`Nombre del producto: ${productoData.nombre_producto}, Categoría: ${productoData.categoria}`);

            const fila = tabla.insertRow();
            // Ajusta las siguientes líneas según tu estructura de datos
            const imagenCell = fila.insertCell(0);
            const nombreCell = fila.insertCell(1);
            const descripcionCell = fila.insertCell(2);
            const precioCell = fila.insertCell(3);
            const accionesCell = fila.insertCell(4);

            // Agrega una etiqueta img con su src y alt
            imagenCell.innerHTML = `<img src="${productoData.imagen}" alt="${productoData.nombre_producto}">`;

            // Asigna los valores de los otros campos a las celdas
            nombreCell.textContent = productoData.nombre_producto;
            descripcionCell.textContent = productoData.descripcion;
            precioCell.textContent = `$${productoData.precio}`;

            // Agrega botones de acciones (editar, eliminar, etc.) según sea necesario
            accionesCell.innerHTML = `<button style="background-color: #0f0; color:#000;" onclick="editarProduct('${productoData.nombre_producto}', '${productoData.descripcion}', '${productoData.precio}', '${productoData.imagen}','${productoData.categoria}')">Editar</button> <button style="background-color: #f00; color:#000;" onclick="deleteProduct('${productoData.nombre_producto}')">Eliminar</button>            `;
        });
    } catch (error) {
        console.error("Error al cargar datos en la tabla:", error);
    }
};

const MSJOk = () => {
    Swal.fire({
        title: "Todo en orden!",
        text: "Producto Eliminado Correctamenrte!",
        icon: "success"
    });
}

const MSJERR = () => {
    Swal.fire({
        title: "Ooops!",
        text: "Eror al Eliminar el Producto!",
        icon: "error"
    });
}

// Función para editar un producto
window.editarProduct = function (nombre, descripcion, precio, imagen, categoria) {
    // Crea un objeto con los datos del producto
    const producto = {
        nombre: nombre,
        descripcion: descripcion,
        precio: precio,
        imagen: imagen,
        categoria: categoria
    };

    // Almacena los datos del producto en localStorage
    localStorage.setItem('productoData', JSON.stringify(producto));

    // Redirige al usuario a la página editarProducto.html con los datos como parámetros
    const params = new URLSearchParams();
    params.append('nombre', nombre);
    params.append('descripcion', descripcion);
    params.append('precio', precio);
    params.append('imagen', imagen);
    params.append('categoria', categoria);

    window.location.href = `../html/editarProducto.html?${params.toString()}`;
};

//Funcion para eliminar un producto de la base de datos
// Función para eliminar un producto por nombre
window.deleteProduct = function (nombre) {
    // Obtén la referencia a la colección "productos" en Firestore
    const productosCollection = collection(getFirestore(), 'productos');

    // Consulta el producto por nombre
    const q = query(productosCollection, where('nombre_producto', '==', nombre));

    // Ejecuta la consulta y maneja la promesa resultante
    getDocs(q)
        .then(querySnapshot => {
            // Verifica si existe el producto en la base de datos
            if (!querySnapshot.empty) {
                // Obtiene el documento del primer resultado
                const productDoc = querySnapshot.docs[0];

                // Elimina el documento de la base de datos
                deleteDoc(productDoc.ref)
                    .then(() => {
                        console.log('Producto eliminado correctamente');
                        MSJOk();
                        window.location.href = '../html/panelAdmin.html';
                    })
                    .catch(error => {
                        console.error('Error al eliminar el producto:', error);
                        MSJERR();
                    });
            } else {
                MSJERR();
                console.error(`No se encontró el producto "${nombre}" en la base de datos.`);
            }
        })
        .catch(error => {
            console.error('Error en la función deleteProduct:', error);
            MSJERR();
        });
};

// Llama a la función para cargar los datos en cada tabla
cargarDatosEnTabla('bebida', 'tblBebidas');
cargarDatosEnTabla('alimento', 'tblAlimentos');
cargarDatosEnTabla('postre', 'tblPostres');
