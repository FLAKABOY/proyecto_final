// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getFirestore, collection, getDocs, query, where } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-firestore.js";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCoTj2mBI0sOYPDsRErcAIyrRI5VYKRfqU",
    authDomain: "proyecto-final-6cba4.firebaseapp.com",
    projectId: "proyecto-final-6cba4",
    storageBucket: "proyecto-final-6cba4.appspot.com",
    messagingSenderId: "787498874785",
    appId: "1:787498874785:web:ad512c98257b0eb03c3de0"
};

// Initialize Firebase
const firebase = initializeApp(firebaseConfig);
const db = getFirestore(firebase);

// Función para cargar los datos en secciones de cuadrados
const cargarDatosEnSecciones = async (categoria, sectionId) => {
    console.log(`Cargando datos para la categoría: ${categoria}`);
    try {
        // Consulta la colección de productos para la categoría específica
        const productosCollection = collection(db, 'productos');
        const productosQuery = query(productosCollection, where('categoria', 'array-contains', categoria));
        const productosSnapshot = await getDocs(productosQuery);

        console.log(`Número de productos encontrados para ${categoria}: ${productosSnapshot.size}`);

        // Obtiene la sección HTML por su ID
        const section = document.getElementById(sectionId);
        // Limpia la sección antes de agregar nuevos datos
        //section.innerHTML = '';

        // Itera sobre los documentos de la consulta y agrega cuadrados a la sección
        productosSnapshot.forEach((doc) => {
            const productoData = doc.data();
            console.log(`Nombre del producto: ${productoData.nombre_producto}, Categoría: ${productoData.categoria}`);

            // Crea un cuadrado para cada producto
            const cuadrado = document.createElement('div');
            cuadrado.classList.add('card');

            cuadrado.innerHTML = `
                <div class="card__image-holder">
                    <img class="card__image" src="${productoData.imagen}" alt="${productoData.nombre_producto}">
                </div>
                <div class="card-title">
                    <a href="#" class="toggle-info btn">
                        <span class="left"></span>
                        <span class="right"></span>
                    </a>
                    <h2>
                        ${productoData.nombre_producto}
                    </h2>
                </div>
                <div class="card-flap flap1">
                    <div class="card-description">
                        ${productoData.descripcion}
                        <center>
                        <p>$${productoData.precio}</p>
                        </center>
                    </div>
                    <div class="card-flap flap2">
                        <div class="card-actions">
                            <a href="#" class="btn" onclick="MSJ()">Comprar</a>
                        </div>
                    </div>
                </div>
            `;

            // Agrega el cuadrado a la sección
            section.appendChild(cuadrado);
        });

        // Llama a la función de jQuery después de cargar los datos
        activarEventos();
    } catch (error) {
        console.error("Error al cargar datos en la sección:", error);
    }
};

window.MSJ = function () {
    Swal.fire({
        title: "Orden Exitosa!",
        text: "Producto Adquirido con Exito!",
        icon: "success"
    });
}

// Función para activar eventos con jQuery
const activarEventos = () => {
    $(document).on('click', 'div.card', function (e) {
        e.preventDefault();

        const $card = $(this);

        if ($card.hasClass("show")) {
            // La tarjeta ya está en vista, ocultarla
            $card.removeClass("show");
            $("div.cards").removeClass("showing");
        } else {
            // Ocultar todas las tarjetas mostradas
            $("div.card.show").removeClass("show");
            // Mostrar la tarjeta actual
            $card.addClass("show");
            $("div.cards").addClass("showing");
        }
    });
};


// Llama a la función para cargar los datos en la sección de bebidas
cargarDatosEnSecciones('postre', 'postresSection');